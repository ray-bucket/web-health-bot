import { scheduler } from './core/scheduler';
import { Server } from './entities/Server';

export async function scheduleTasks() {
  const servers = await Server.find();

  for (const server of servers) {
    if (server.enabled) {
      scheduler.create(server);
    }
  }
}
