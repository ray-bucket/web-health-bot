import { Markup } from 'telegraf';

export const intervalButtons = Markup.inlineKeyboard([
  [
    Markup.callbackButton('1 min', '1'),
    Markup.callbackButton('5 min', '5'),
  ],
  [
    Markup.callbackButton('10 min', '10'),
    Markup.callbackButton('15 min', '15'),
  ],
  [
    Markup.callbackButton('30 min', '30'),
    Markup.callbackButton('1 hr', '60'),
  ],
]);
