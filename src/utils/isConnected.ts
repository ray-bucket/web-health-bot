import * as dns from 'dns';

export function isConnected() {
  return new Promise<boolean>((resolve) => {
    dns.resolve('www.google.com', (error) => {
      if (error) {
        resolve(false);
      } else {
        resolve(true);
      }
    });
  });
}
