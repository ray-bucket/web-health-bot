import { User } from '../entities/User';

export async function createUser(id: number): Promise<User> {
  const user = await User.findOne({
    where: { id },
  });
  if (user) {
    return user;
  }

  const newUser = new User();
  newUser.id = id;
  await newUser.save();

  return newUser;
}
