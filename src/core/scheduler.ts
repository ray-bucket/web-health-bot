import { EventEmitter } from 'events';
import { schedule, ScheduledTask } from 'node-cron';
import { Server } from '../entities/Server';

export interface HealthTask {
  schedule: ScheduledTask;
  abort?: () => void;
}

const tasks = new Map<number, HealthTask>();
const emmiter = new EventEmitter();

function create(server: Server) {
  const task = schedule(`*/${server.interval} * * * *`, async () => {
    if (await Server.count({ where: { id: server.id }, take: 1 }) === 0) return;
    await server.reload();
    emmiter.emit('trigger', server, task);
  });

  tasks.set(server.id, { schedule: task });

  // trigger all the first time
  emmiter.emit('trigger', server, task);

  return task;
}

function remove(id: number) {
  const task = tasks.get(id);
  if (!task) return;

  task.schedule.destroy();
  if (task.abort) {
    task.abort();
  }
  return task;
}

function onTrigger(
  handler: (server: Server, task: HealthTask) => void,
) {
  const listener = (...args: any[]) => (handler as any)(...args);
  emmiter.on('trigger', handler);

  return () => {
    emmiter.off('trigger', listener);
  };
}

function setAbortRequest(serverId: number, abort: () => void) {
  const task = tasks.get(serverId);
  if (!task) {
    console.log(`can not set abort function to "${serverId}", task not found`);
    return;
  }
  task.abort = abort;
}

export const scheduler = {
  create,
  remove,
  onTrigger,
  setAbortRequest,
};
