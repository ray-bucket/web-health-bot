import fetch, {} from 'node-fetch';
import AbortController from 'abort-controller';
import { Telegram } from 'telegraf';
import { Server } from '../entities/Server';
import { HealthTask } from './scheduler';
import { isConnected } from '../utils/isConnected';

const TIMEOUT = 30000;

export async function checkHealth(server: Server, task: HealthTask, telegram: Telegram) {
  const hasConnection = await isConnected();
  if (!hasConnection) {
    return;
  }

  const notifyError = async (error: string) => {
    if (await Server.count({ where: { id: server.id }, take: 1 }) === 0) return;
    await server.reload();
    telegram.sendMessage(
      server.userId,
      `<b>Health alert on ${server.url}</b>\n<code>${error}</code>`,
      { parse_mode: 'HTML' },
    );
  };

  const controller = new AbortController();
  // eslint-disable-next-line no-param-reassign
  task.abort = () => {
    controller.abort();
  };

  try {
    const response = await fetch(server.url, {
      timeout: TIMEOUT,
      signal: controller.signal,
    });
    if (response.status < 200 || response.status > 299) {
      notifyError(`${response.statusText} (${response.status})`);
    }
  } catch (error) {
    if (error.name !== 'AbortError') {
      notifyError(error.message || 'Unkown error');
    }
  }
}
