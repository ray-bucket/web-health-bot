import { Like } from 'typeorm';
import { Server } from '../entities/Server';

export interface SererPaginationOptions {
  userId: number;
  page: number;
  filter?: string;
}

export interface ServerPaginationResult {
  first: number;
  prev: number | null;
  next: number | null;
  last: number;
  items: Server[];
}

export type ServerPagination = (options: SererPaginationOptions) => Promise<ServerPaginationResult>;

const PAGE_ELEMENTS = 5;

export const serverPagination: ServerPagination = async ({ page, userId, filter = '' }) => {
  const serversCounter = await Server.count({
    where: { userId, url: Like(`%${filter}%`.toLowerCase()) },
  });

  const pages = Math.ceil(serversCounter / PAGE_ELEMENTS);

  const servers = await Server.find({
    where: { userId, url: Like(`%${filter}%`.toLowerCase()) },
    order: { url: 'ASC' },
    skip: page * PAGE_ELEMENTS,
    take: PAGE_ELEMENTS,
  });

  return {
    first: 0,
    prev: page <= 0 ? 0 : page - 1,
    next: page + 1 >= pages ? page : page + 1,
    last: pages - 1,
    items: servers,
  };
};
