import {
  BaseEntity, Entity, OneToMany, PrimaryColumn,
} from 'typeorm';
import { Server } from './Server';

@Entity()
export class User extends BaseEntity {
  @PrimaryColumn({ type: 'int' })
  id: number;

  @OneToMany(() => Server, (server) => server.user)
  servers: Server[];
}
