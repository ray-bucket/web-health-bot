import { Server } from './Server';
import { User } from './User';

export const entities = [Server, User];
