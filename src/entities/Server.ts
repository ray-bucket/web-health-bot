import {
  BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './User';

export interface IServer {
  id: number;
  url: string;
  interval: number;
  enabled: boolean;
}

@Entity()
export class Server extends BaseEntity implements IServer {
  @PrimaryGeneratedColumn({ type: 'int' })
  public id: number;

  @Column({ type: 'boolean', default: true })
  public enabled: boolean;

  @Column({ type: 'text' })
  public url: string;

  @Column({ type: 'int' })
  public interval: number;

  @Column({ type: 'int' })
  public userId: number;

  @ManyToOne(() => User, (user) => user.servers)
  public user: User;
}
