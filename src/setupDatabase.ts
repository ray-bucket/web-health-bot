import { createConnection } from 'typeorm';
import { entities } from './entities';

export function setupDatabase() {
  return createConnection({
    type: 'sqlite',
    database: 'db.sqlite',
    synchronize: true,
    logging: false,
    entities,
  });
}
