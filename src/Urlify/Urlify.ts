function serializeQuery(query: any): string {
  const result = Object.keys(query)
    .map((key) => `${key}=${query[key]}`)
    .join('&');

  return result;
}

function deserializeQuery(query: string) {
  const fixedQuery = query.startsWith('?') ? query.substring(1) : '';
  return fixedQuery.split('&')
    .map((segment) => segment.split('='))
    .filter(([segment]) => segment)
    .reduce((params, [key, value = '']) => ({ ...params, [key]: value }), {});
}

export function Urlify(baseUrl?: string) {
  const pattern = /^(\w+?:\/\/)?([^:/]+)?([^\w\-..]+[^?#]+)(\?[^#]+)?(#.*)?/i;
  const [,
    protocol = '',
    host = '',
    path = '',
    query = '',
    fragment = '',
  ] = baseUrl ? pattern.exec(baseUrl) : [];

  const queryParams: object = query
    .substr(1)
    .split('&')
    .map((segment) => segment.split('='))
    .filter(([segment]) => segment)
    .reduce((params, [key, value = '']) => ({ ...params, [key]: value }), {});

  return {
    protocol,
    host,
    path,
    query,
    fragment,
    queryParams,
    get queryString() {
      return serializeQuery(this.queryParams);
    },
    set queryString(newQuery) {
      this.queryParams = deserializeQuery(newQuery);
    },
    toString() {
      const { queryString } = this;
      return `${this.protocol}${this.host}${this.path}${queryString ? `?${queryString}` : ''}${this.fragment}`;
    },
  };
}
