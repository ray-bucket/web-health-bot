import { session, Stage, Telegraf } from 'telegraf';
import { checkHealth } from '../core/checkHealth';
import { createUser } from '../core/createUser';
import { scheduler } from '../core/scheduler';
import { router, serverListRoute } from './routes';
import wizards from './wizards';

export async function setupTelegram() {
  const bot = new Telegraf(process.env.TELEGRAM_TOKEN);
  const stage = new Stage(wizards);

  bot.catch((param: any) => {
    console.error(param);
    process.exit(-1);
  });
  bot.use(session());
  bot.use(router.middleware());
  bot.use(stage.middleware() as any);
  bot.use(async (ctx, next) => {
    await createUser(ctx.chat.id);
    next();
  });

  bot.start(async (ctx) => {
    ctx.reply(`Hi ${ctx.chat.first_name}, I can help you to schedule requests to check server health, if something goes wrong I will notify you :)`);
  });
  bot.help(async (ctx) => {
    ctx.reply('You can write a part of a server url and i will search and show');
  });
  bot.command('list', (ctx) => {
    serverListRoute.reply(ctx);
  });
  bot.command('new', (ctx) => {
    // @ts-ignore
    ctx.scene.enter('NEW_SERVER_WIZARD');
  });
  bot.on('message', (ctx) => {
    serverListRoute.reply(ctx, {
      filter: ctx.message.text.trim(),
    });
  });

  scheduler.onTrigger((server, task) => {
    checkHealth(server, task, bot.telegram);
  });

  await bot.launch();
}
