import { IServer, Server } from '../../entities/Server';
import { scheduler } from '../../core/scheduler';
import { WizardContext } from '../../types';
import { intervalButtons } from '../../utils/inlineButtons';
import { isValidUrl } from '../../utils/isValidUrl';

const WizardScene = require('telegraf/scenes/wizard');

export const newServerWizard = new WizardScene(
  'NEW_SERVER_WIZARD',
  (ctx: WizardContext) => {
    ctx.reply('What is the server url?');
    ctx.wizard.state.server = {};
    return ctx.wizard.next();
  },
  (ctx: WizardContext) => {
    if (!ctx.message) return;

    // check for cancel command
    if (ctx.message.text === '/cancel') {
      ctx.reply('Operation cancelled');
      ctx.scene.leave();
      return;
    }

    // validate input
    if (!isValidUrl(ctx.message.text)) {
      ctx.reply('Please send me a valid url');
      return;
    }

    // - - - - - next step
    const { server } = ctx.wizard.state as { server: IServer };
    server.url = ctx.message.text;

    ctx.reply(
      'Select a requests interval or type it (in minutes)',
      { reply_markup: intervalButtons },
    );

    return ctx.wizard.next();
  },
  async (ctx: WizardContext) => {
    if (ctx.message?.text === '/cancel') {
      ctx.reply('Operation cancelled');
      ctx.scene.leave();
      return;
    }

    if (ctx.callbackQuery) {
      ctx.answerCbQuery();
    }

    const input = ctx.callbackQuery?.data || ctx.message?.text;
    if (!input.trim() || Number.isNaN(input) || Number(input) <= 0) {
      ctx.reply(
        'Invalid number, please send me a valid number or select an option',
        { reply_markup: intervalButtons },
      );
      return;
    }

    const { server } = ctx.wizard.state as { server: IServer };
    server.interval = Number(input);

    // - - - - - save
    const newServer = await Server.create({
      ...server,
      userId: ctx.chat.id,
    }).save();

    scheduler.create(newServer);
    ctx.reply('The server has been saved');

    return ctx.scene.leave();
  },
);
