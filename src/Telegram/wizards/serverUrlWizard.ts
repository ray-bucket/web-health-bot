import { scheduler } from '../../core/scheduler';
import { Server } from '../../entities/Server';
import { WizardContext } from '../../types';
import { isValidUrl } from '../../utils/isValidUrl';
import { serverDetailsRoute } from '../routes';

const WizardScene = require('telegraf/scenes/wizard');

export const serverUrlWizard = new WizardScene(
  'SERVER_URL_WIZARD',
  (ctx: WizardContext) => {
    ctx.reply('What is the new server url?');
    return ctx.wizard.next();
  },
  async (ctx: WizardContext) => {
    // @ts-ignore
    const { serverId } = ctx.session;

    if (ctx.message?.text === '/cancel') {
      ctx.reply('Operation cancelled');
      ctx.scene.leave();
      serverDetailsRoute.reply(ctx, { id: serverId });

      return;
    }

    // validate input
    const input = (ctx.message?.text || '').trim().toLowerCase();
    if (!isValidUrl(input)) {
      // TODO: wtf?
      console.log('asdasd');
    }

    // - - - - - save

    const server = await Server.findOne({ id: serverId });
    server.url = input;
    await server.save();

    if (server.enabled) {
      scheduler.remove(server.id); // delete old task
      scheduler.create(server); // use new interval to schedule a new task
    }

    ctx.scene.leave();
    serverDetailsRoute.reply(ctx, { id: server.id });
  },
);
