import { scheduler } from '../../core/scheduler';
import { Server } from '../../entities/Server';
import { WizardContext } from '../../types';
import { intervalButtons } from '../../utils/inlineButtons';
import { serverDetailsRoute } from '../routes';

const WizardScene = require('telegraf/scenes/wizard');

export const severIntervalWizard = new WizardScene(
  'SERVER_INTERVAL_WIZARD',
  (ctx: WizardContext) => {
    ctx.reply(
      'Select a new requests interval or type it (in minutes)',
      { reply_markup: intervalButtons },
    );
    return ctx.wizard.next();
  },
  async (ctx: WizardContext) => {
    // @ts-ignore
    const { serverId } = ctx.session;

    if (ctx.message?.text === '/cancel') {
      ctx.reply('Operation cancelled');
      ctx.scene.leave();
      serverDetailsRoute.reply(ctx, { id: serverId });

      return;
    }

    if (ctx.callbackQuery) {
      ctx.answerCbQuery();
    }

    const input = ctx.callbackQuery?.data || ctx.message?.text;
    if (!input.trim() || Number.isNaN(input) || Number(input) <= 0) {
      ctx.reply(
        'Invalid number, please send me a valid number or select an option',
        { reply_markup: intervalButtons },
      );
      return;
    }

    // - - - - - save

    const server = await Server.findOne({ id: serverId });
    server.interval = Number(input);
    await server.save();

    if (server.enabled) {
      scheduler.remove(server.id); // delete old task
      scheduler.create(server); // use new interval to schedule a new task
    }

    ctx.scene.leave();
    serverDetailsRoute.reply(ctx, { id: server.id });
  },
);
