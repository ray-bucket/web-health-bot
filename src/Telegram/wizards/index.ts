import { newServerWizard } from './newServerWizard';
import { severIntervalWizard } from './serverIntervalWizard';
import { serverUrlWizard } from './serverUrlWizard';

const wizards = [
  newServerWizard,
  severIntervalWizard,
  serverUrlWizard,
];

export default wizards;
