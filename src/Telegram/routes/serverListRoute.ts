import { Markup } from 'telegraf';
import { router, serverDetailsRoute } from '.';
import { serverPagination } from '../../core/serverPagination';

export const serverListRoute = router.route('/servers', async (ctx, params) => {
  const page = Number(params.page) || 0;
  const filter = params.filter || '';
  const userId = ctx.chat.id;

  const pagination = await serverPagination({ page, userId, filter });
  if (pagination.items.length === 0) {
    if (filter) {
      return ['No results', null];
    }
    return ['You don\'t have servers, use the /new command to add a new server', null];
  }

  const buttons = pagination.items.map((server) => [
    Markup.callbackButton(server.url, serverDetailsRoute.path({ id: server.id })),
  ]);

  if (pagination.last > 0) {
    buttons.push([
      Markup.callbackButton('«', serverListRoute.path({ page: pagination.first })),
      Markup.callbackButton('‹', serverListRoute.path({ page: pagination.prev })),
      Markup.callbackButton('›', serverListRoute.path({ page: pagination.next })),
      Markup.callbackButton('»', serverListRoute.path({ page: pagination.last })),
    ]);
  }

  return [
    'Chose a server',
    { reply_markup: Markup.inlineKeyboard(buttons) },
  ];
});
