import { Markup } from 'telegraf';
import { Server } from '../../entities/Server';
import {
  router, deleteServerRoute, serverListRoute, serverUrlRoute, serverIntervalRoute,
} from '.';
import { toggleServerEnabledRoute } from './toggleServerEnabledRoute';

export const serverDetailsRoute = router.route('/server', async (ctx, params) => {
  const { id } = params;
  const server = await Server.findOne(id);

  if (!server) {
    const buttons = Markup.inlineKeyboard([
      [Markup.callbackButton('« Go back', serverListRoute.path())],
    ]);
    return [
      'Server not found :(',
      { reply_markup: buttons },
    ];
  }

  const toggleText = server.enabled ? 'Disable' : 'Enable';

  const buttons = Markup.inlineKeyboard([
    [
      Markup.callbackButton('Change url', serverUrlRoute.path({ id })),
      Markup.callbackButton('Change interval', serverIntervalRoute.path({ id })),
    ],
    [
      Markup.callbackButton('Delete', deleteServerRoute.path({ id })),
      Markup.callbackButton(toggleText, toggleServerEnabledRoute.path({ id })),
    ],
    [Markup.callbackButton('« Go back', serverListRoute.path())],
  ]);

  const lines = [
    `<b>Url</b>: ${server.url}`,
    `<b>Interval</b>: ${server.interval} min.`,
    `<b>Enabled</b>: ${server.enabled}`,
  ];

  return [
    lines.join('\n'),
    { reply_markup: buttons, parse_mode: 'HTML' },
  ];
});
