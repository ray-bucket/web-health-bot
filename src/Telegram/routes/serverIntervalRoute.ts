import { router, serverListRoute } from '.';
import { Server } from '../../entities/Server';

export const serverIntervalRoute = router.route('/server-interval', async (ctx, params) => {
  const { id } = params;
  const server = await Server.findOne({ id });

  if (!server) {
    serverListRoute.redirect(ctx, params);
    return;
  }

  // @ts-ignore
  ctx.session.serverId = Number(id);

  // @ts-ignore
  ctx.scene.enter('SERVER_INTERVAL_WIZARD');
});
