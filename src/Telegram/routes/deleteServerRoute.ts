import { Markup } from 'telegraf';
import { router, serverDetailsRoute, serverListRoute } from '.';
import { scheduler } from '../../core/scheduler';
import { Server } from '../../entities/Server';

export const confirmServerDeletionRoute = router.route('/confirm-server-deletion', async (ctx, params) => {
  const id = Number(params.id);

  await Server.delete(id);
  scheduler.remove(id);

  serverListRoute.redirect(ctx);
});

export const deleteServerRoute = router.route('/delete-server', (ctx, params) => {
  const { id } = params;

  const buttons = Markup.inlineKeyboard([
    [
      Markup.callbackButton('Yes', confirmServerDeletionRoute.path({ id })),
      Markup.callbackButton('No', serverDetailsRoute.path({ id })),
    ],
  ]);

  return [
    'Are you sure to delete ?',
    { reply_markup: buttons },
  ];
});
