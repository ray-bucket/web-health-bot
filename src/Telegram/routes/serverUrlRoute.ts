import { router, serverListRoute } from '.';
import { Server } from '../../entities/Server';

export const serverUrlRoute = router.route('/server-url', async (ctx, params) => {
  const { id } = params;
  const server = await Server.findOne({ id });

  if (!server) {
    ctx.reply('Server not found');
    serverListRoute.reply(ctx, params);
    return;
  }

  // @ts-ignore
  ctx.session.serverId = Number(id);

  // @ts-ignore
  ctx.scene.enter('SERVER_URL_WIZARD');
});
