import { router, serverListRoute, serverDetailsRoute } from '.';
import { scheduler } from '../../core/scheduler';
import { Server } from '../../entities/Server';

export const toggleServerEnabledRoute = router.route('/disable-server-route', async (ctx, params) => {
  const { id } = params;
  const server = await Server.findOne({ where: { id } });

  if (!server) {
    ctx.reply('Server not found');
    serverListRoute.redirect(ctx);
    return;
  }

  server.enabled = !server.enabled;
  if (server.enabled) {
    scheduler.create(server);
  } else {
    scheduler.remove(server.id);
  }

  await server.save();

  serverDetailsRoute.redirect(ctx, { id });
});
