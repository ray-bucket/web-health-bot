import { TelegrafContext } from 'telegraf/typings/context';

export interface WizardContext extends TelegrafContext {
  wizard: {
    state: any,
    next: Function;
  };
  scene: {
    leave: Function;
  };
}
