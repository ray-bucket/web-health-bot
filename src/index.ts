import { scheduleTasks } from './scheduleTasks';
import { setupDatabase } from './setupDatabase';
import { setupTelegram } from './Telegram';

async function main() {
  console.log('Setup database');
  await setupDatabase();

  console.log('Setup Telegram');
  await setupTelegram();

  console.log('Schedule tasks');
  await scheduleTasks();

  console.log('Ready...');
}

main();
