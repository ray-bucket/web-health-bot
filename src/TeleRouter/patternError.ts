export function patternError(name: string, value: string, pattern: RegExp) {
  console.error(`The ${name} "${value}" does not math with "${pattern}"`);
}
