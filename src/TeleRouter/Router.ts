import { Middleware } from 'telegraf';
import { TelegrafContext } from 'telegraf/typings/context';
import { ExtraReplyMessage } from 'telegraf/typings/telegram-types';
import { Urlify } from '../Urlify';
import { patternError } from './patternError';

export type RouteHandlerResponse = string
| ExtraReplyMessage
| [string, ExtraReplyMessage];

export type RouteHandler = (
  ctx: TelegrafContext,
  params: any,
) => Promise<RouteHandlerResponse | void> | RouteHandlerResponse | void;

export type RouteCaller = (
  ctx: TelegrafContext,
  params?: any,
) => Promise<void> | void;

export interface Route {
  path: (params?: any) => string;
  reply: RouteCaller;
  redirect: RouteCaller;
}

export type RutesMap = Record<string, Route>;

export class Router {
  private routes: RutesMap = {};

  private resolve(query: string, ctx: TelegrafContext) {
    const url = Urlify(query);
    const route = this.routes[url.path];

    if (!route) {
      console.error(`path not found "${url.path}"`);
      return;
    }
    route.redirect(ctx, url.queryParams);
  }

  public route(path: string, handler: RouteHandler): Route {
    const pathPattern = /^(?:\/[\w-+]+)+$/;
    if (!pathPattern.test(path)) {
      patternError('path', path, pathPattern);
      return;
    }

    const route: Route = {
      path: (params = {}) => {
        const url = Urlify(path);
        Object.assign(url.queryParams, params);

        return url.toString();
      },
      reply: async (ctx, params = {}) => {
        const result = await handler(ctx, params);
        if (!result) return;

        if (typeof result === 'string') {
          ctx.reply(result);
        } else if (result instanceof Array) {
          ctx.reply(...result);
        } else {
          ctx.reply('', result);
        }
      },
      redirect: async (ctx, params = {}) => {
        const result = await handler(ctx, params);
        if (!result) return;

        if (typeof result === 'string') {
          ctx.editMessageText(result);
        } else if (result instanceof Array) {
          ctx.editMessageText(...result);
        } else {
          ctx.editMessageText('', result);
        }

        if (ctx.callbackQuery) {
          ctx.answerCbQuery();
        }
      },
    };

    this.routes[path] = route;

    return route;
  }

  public middleware(): Middleware<TelegrafContext> {
    return async (ctx, next) => {
      const query = ctx.callbackQuery?.data;
      const pattern = /^(\/[\w-]+)+\??/;
      if (!query || !pattern.test(query)) return next();

      await this.resolve(query, ctx);
      return next();
    };
  }
}
